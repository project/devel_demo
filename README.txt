Devel Demo
================================================================================

Summary
--------------------------------------------------------------------------------

A demonstration of most of the tools from the Devel module. This module is a
companion to the Debugging Drupal guide available at:

  http://ratatosk.backpackit.com/pub/1836982-debugging-drupal


Requirements
--------------------------------------------------------------------------------

The Devel Demo module requires the following modules:

   Devel
   Drupal for Firebug (optional)


Installation
--------------------------------------------------------------------------------

1. Copy the devel_demo folder to sites/all/modules or to a site-specific
   modules folder.

2. Go to Modules and enable the Devel Demo module.


Configuration
--------------------------------------------------------------------------------

There is nothing to configure. Go to Administer > Configuration > Development >
Devel Demo.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/devel_demo


Credits
--------------------------------------------------------------------------------

Author: Morten Wulff <wulff@ratatosk.net>


